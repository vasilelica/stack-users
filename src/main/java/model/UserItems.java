package model;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserItems {

	@SerializedName("items")
	@Expose
	private List<User> users = null;
	@SerializedName("has_more")
	@Expose
	private Boolean hasMore;
	@SerializedName("quota_max")
	@Expose
	private Long quotaMax;
	@SerializedName("quota_remaining")
	@Expose
	private Long quotaRemaining;
	@SerializedName("total")
	@Expose
	private Long total;

	public List<User> getUsers() {
		return users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}

	public Boolean getHasMore() {
		return hasMore;
	}

	public void setHasMore(Boolean hasMore) {
		this.hasMore = hasMore;
	}

	public Long getQuotaMax() {
		return quotaMax;
	}

	public void setQuotaMax(Long quotaMax) {
		this.quotaMax = quotaMax;
	}

	public Long getQuotaRemaining() {
		return quotaRemaining;
	}

	public void setQuotaRemaining(Long quotaRemaining) {
		this.quotaRemaining = quotaRemaining;
	}

	public Long getTotal() {
		return total;
	}

	public void setTotal(Long total) {
		this.total = total;
	}

}
