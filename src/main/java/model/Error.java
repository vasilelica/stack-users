package model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Error {

	@SerializedName("error_id")
	@Expose
	private Long errorId;

	@SerializedName("error_message")
	@Expose
	private String description;

	@SerializedName("error_name")
	@Expose
	private String errorName;

	public Long getErrorId() {
		return errorId;
	}

	public void setErrorId(Long errorId) {
		this.errorId = errorId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getErrorName() {
		return errorName;
	}

	public void setErrorName(String errorName) {
		this.errorName = errorName;
	}

	@Override
	public String toString() {
		return "{" +
				"errorId=" + errorId +
				", description='" + description + '\'' +
				", errorName='" + errorName + '\'' +
				'}';
	}

}