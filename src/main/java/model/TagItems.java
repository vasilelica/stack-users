package model;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TagItems {

	@SerializedName("items")
	@Expose
	private List<Tag> tags = null;

	public List<Tag> getItems() {
		return tags;
	}

	public void setItems(List<Tag> items) {

		this.tags = items;
	}

}