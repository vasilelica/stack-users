package model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class User {

	@SerializedName("answer_count")
	@Expose
	private Long answerCount;
	@SerializedName("question_count")
	@Expose
	private Long questionCount;
	@SerializedName("reputation")
	@Expose
	private Long reputation;
	@SerializedName("user_id")
	@Expose
	private Long userId;
	@SerializedName("location")
	@Expose
	private String location;
	@SerializedName("link")
	@Expose
	private String link;
	@SerializedName("profile_image")
	@Expose
	private String profileImage;
	@SerializedName("display_name")
	@Expose
	private String displayName;

	private List<String> tags = new ArrayList<>();

	public List<String> getTags() {
		return tags;
	}

	public void setTags(List<String> tags) {
		this.tags = tags;
	}

	public void addTag(String tag) {
		this.tags.add(tag);
	}

	public Long getAnswerCount() {
		return answerCount;
	}

	public void setAnswerCount(Long answerCount) {
		this.answerCount = answerCount;
	}

	public Long getQuestionCount() {
		return questionCount;
	}

	public void setQuestionCount(Long questionCount) {
		this.questionCount = questionCount;
	}

	public Long getReputation() {
		return reputation;
	}

	public void setReputation(Long reputation) {
		this.reputation = reputation;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public String getProfileImage() {
		return profileImage;
	}

	public void setProfileImage(String profileImage) {
		this.profileImage = profileImage;
	}

	@Override
	public String toString() {
		String.join(",", tags);

		return "'" + displayName + '\'' +
				", '" + location + '\'' +
				",  " + answerCount +
				",  " + questionCount +
				",  '" + tags + '\'' +
				", '" + link + '\'' +
				", '" + profileImage + '\''
				;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

}