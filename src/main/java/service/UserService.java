package service;

import client.RetrofitApiClient;
import model.User;
import model.UserItems;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import utils.ApiUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.joining;

public class UserService {

	public static Map<Long, User> filteredUsersWithTags = new HashMap<>();
	private Integer fromPage;
	private Integer toPage;
	public boolean hasMoreUsers = true;
	private RetrofitApiClient apiClient;
	private TagService tagService;

	public UserService(RetrofitApiClient apiClient, TagService tagService, Integer fromPage, Integer toPage) {
		this.fromPage = fromPage;
		this.apiClient = apiClient;
		this.tagService = tagService;
		this.toPage = toPage;
	}

	public void collectUsers() {
		Call<UserItems> httpCall;
		try {
			while (hasMoreUsers && fromPage < toPage) {
				httpCall = apiClient.getUserApi().getUserItems(fromPage, ApiUtils.ACCESS_TOKEN,
						ApiUtils.USER_FILTER, ApiUtils.PAGE_SIZE, ApiUtils.KEY);
				Thread.sleep(23000);


				httpCall.enqueue(new Callback<UserItems>() {
					@Override
					public void onResponse(Call<UserItems> call, Response<UserItems> response) {

						if (response.isSuccessful()) {
							List<User> filteredUsers = getFilteredUsers(response.body(), fromPage);

							Map<Long, User> mappedUsers = mapUsersIntoMap(filteredUsers);
							String userIdsDelimited = delimitUserIds(filteredUsers);

							if (!userIdsDelimited.isEmpty()) {
								tagService.checkUsersTags(userIdsDelimited, mappedUsers, fromPage);
							}

							hasMoreUsers = response.body().getHasMore();
							fromPage++;

						} else {
							ApiUtils.checkAndPrintTheError(response, fromPage);
							if (response.body() != null) {
								hasMoreUsers = response.body().getHasMore();
							}
							fromPage++;
						}
					}

					@Override
					public void onFailure(Call<UserItems> call, Throwable t) {
						System.out.println("Retrofit can't reach the stack exchange API, error: " + t.getMessage());
					}
				});
			}
		} catch (InterruptedException e) {
			System.out.println("InterruptedException occurred");
			e.printStackTrace();
		}
	}

	public Map<Long, User> mapUsersIntoMap(List<User> users) {
		return users.stream().collect(
				Collectors.toMap(User::getUserId, Function.identity())
		);
	}

	private List<User> getFilteredUsers(UserItems userItems, Integer pageCounter) {
		List<User> filterUsers = userItems.getUsers().stream()
				.filter(it -> it.getLocation() != null)
				.filter(it -> it.getLocation().contains("Moldova") || it.getLocation().contains("Romania"))
				.filter(it -> it.getAnswerCount() >= 1)
				.collect(Collectors.toList());
		return filterUsers;
	}

	public String delimitUserIds(List<User> users) {
		String usersIds = users.stream()
				.map(user -> String.valueOf(user.getUserId()))
				.collect(joining(";"));
		return usersIds;
	}
}
