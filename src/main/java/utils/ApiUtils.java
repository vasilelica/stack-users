package utils;

import com.google.gson.Gson;
import model.Error;
import retrofit2.Response;

public class ApiUtils {
	public final static String ACCESS_TOKEN = "Xz7lglM2AL61jDTprSgFiA))";
	public final static String KEY = "sprTLDooVyzLgnz8CJK85w((";
	/* USER_FILTER already filters users with reputation min 223 */
	public final static String USER_FILTER = "!BTeB3PZ3AJzwPaeY_7TGXrRhO0MRe4";
	public final static String TAG_FILTER = "!*MM5FYEgIysr0)KB";
	public final static String PAGE_SIZE = "100";

	public static Gson gson = new Gson();

	public static void checkAndPrintTheError(Response response, Integer pageCounter) {
		System.out.println("pageCounter " + pageCounter);
		Error errorResponse = ApiUtils.gson.fromJson(response.errorBody().charStream(), Error.class);
		System.out.println("Error code: " + errorResponse.getErrorName() + ", error message: " + errorResponse.getDescription());
	}
}