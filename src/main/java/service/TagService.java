package service;

import client.RetrofitApiClient;
import model.Tag;
import model.TagItems;
import model.User;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import utils.ApiUtils;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static service.UserService.*;

public class TagService {
	private RetrofitApiClient apiClient;

	public TagService(RetrofitApiClient apiClient) {
		this.apiClient = apiClient;
	}

	public void checkUsersTags(String userIdsDelimeted, Map<Long, User> mappedUsers, Integer pageCounter) {
		Call<TagItems> httpCall = apiClient.getUserApi().getUserTagItems(userIdsDelimeted, ApiUtils.ACCESS_TOKEN, ApiUtils.TAG_FILTER,
				ApiUtils.PAGE_SIZE, ApiUtils.KEY);


		httpCall.clone().enqueue(new Callback<TagItems>() {
			@Override
			public void onResponse(Call<TagItems> call, Response<TagItems> response) {
				if (response.isSuccessful()) {
					List<Tag> tagItems = response.body().getItems();
					List<Tag> requiredTags = tagItems.stream()
							.filter(tag -> tag.getName().equalsIgnoreCase("java") ||
									tag.getName().equalsIgnoreCase("c#") ||
									tag.getName().equalsIgnoreCase(".net") ||
									tag.getName().equalsIgnoreCase("docker")).collect(Collectors.toList());

					assignTagstoUsers(requiredTags, mappedUsers);

				} else {
					ApiUtils.checkAndPrintTheError(response, pageCounter);
				}
			}

			@Override
			public void onFailure(Call<TagItems> call, Throwable t) {
				System.out.println("Retrofit can't reach the stack exchange API, error: " + t.getMessage());
			}
		});
	}

	private void assignTagstoUsers(List<Tag> tags, Map<Long, User> mappedUsers) {
		if (!tags.isEmpty()) {
			tags.forEach(it -> {
				User user = mappedUsers.get(it.getUserId());

				if (filteredUsersWithTags.get(it.getUserId()) != null) {
					filteredUsersWithTags.get(it.getUserId()).addTag(it.getName());
				} else {
					user.addTag(it.getName());
					filteredUsersWithTags.put(it.getUserId(), user);
				}
			});
		}
	}


}
