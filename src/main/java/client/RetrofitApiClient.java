package client;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import api.UserApiInterface;

public class RetrofitApiClient {
	private UserApiInterface userService;

	public RetrofitApiClient(String baseUrl) {
		Retrofit retrofit = new Retrofit.Builder()
				.baseUrl(baseUrl)
				.addConverterFactory(GsonConverterFactory.create())
				.build();
		userService = retrofit.create(UserApiInterface.class);
	}

	public UserApiInterface getUserApi() {
		return userService;
	}
}
