package api;

import model.TagItems;
import model.UserItems;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface UserApiInterface {

	@GET("/users?order=asc&min=223&sort=reputation&site=stackoverflow")
	Call<UserItems> getUserItems(@Query("page") int page, @Query("access_token") String token, @Query("filter") String filter,
															 @Query("pagesize") String pagesize, @Query("key") String key);

	@GET("/users/{userId}/tags?&order=asc&sort=name&site=stackoverflow")
	Call<TagItems> getUserTagItems(@Path("userId") String userId, @Query("access_token") String token, @Query("filter") String filter,
																 @Query("pagesize") String pagesize, @Query("key") String key);
}




