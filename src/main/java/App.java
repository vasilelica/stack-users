import client.RetrofitApiClient;
import model.User;
import service.TagService;
import service.UserService;

import java.util.ArrayList;
import java.util.List;

public class App {

	public static void main(String[] args) {
		RetrofitApiClient apiClient = new RetrofitApiClient("https://api.stackexchange.com/2.2/");
		TagService tagService = new TagService(apiClient);
		UserService userService = new UserService(apiClient, tagService, 1, 99);
		userService.collectUsers();

		List<User> stackUsers = new ArrayList<>(UserService.filteredUsersWithTags.values());
		stackUsers.forEach(System.out::println);
	}
}