# Test Challange #

For retrieving users and their tags has been used 2 endpoints from: https://api.stackexchange.com/2.2/  
1) users?order=desc&sort=reputation&site=stackoverflow

2) users/{ids}/tags?order=asc&sort=name&site=stackoverflow
 
# Prerequisites #
**access_token** and **key** are hardcoded in the app. These were obtained manually.

# Limitations on api.stackexchnage.com #
From the documentation:  
 1) The maximum size of any page with items is 100
 
 2) Polling at a *rate faster than once a minute* (for semantically identical requests) is considered **abusive**.

Because of throttles and IP quota policy has been decided to make 1req/~20sec

In the app you can set **start** and **end** of pages where users will be retrieved.
In case of the errors, is displayed original API's error messages, and no other logic. 

I had ran the app from page **1** to page **99** and obtained below output:
``` 
'andrei', 'Bucharest, Romania',  6,  0,  '[java]', 'https://stackoverflow.com/users/1133968/andrei', 'https://i.stack.imgur.com/9VKQQ.jpg?s=128&g=1'
'Raul A.', 'Timișoara, Romania',  23,  1,  '[.net, c#, java]', 'https://stackoverflow.com/users/5741620/raul-a', 'https://www.gravatar.com/avatar/7bea6ff4cd55888dec33c937fc728e43?s=128&d=identicon&r=PG&f=1'
'byteflux', 'Sibiu, Romania',  8,  0,  '[.net, c#]', 'https://stackoverflow.com/users/940730/byteflux', 'https://www.gravatar.com/avatar/82b5b824cbd677da1fe362b5c6011589?s=128&d=identicon&r=PG'
'Clapa Lucian', 'Romania',  32,  2,  '[java]', 'https://stackoverflow.com/users/4155898/clapa-lucian', 'https://i.stack.imgur.com/Dprv7.jpg?s=128&g=1'
'Andrei Fiordean', 'Sibiu, Romania',  16,  3,  '[java]', 'https://stackoverflow.com/users/10103772/andrei-fiordean', 'https://www.gravatar.com/avatar/903f8b4f232388ba5be0d9d7f6cce548?s=128&d=identicon&r=PG&f=1'
'Hobroker', 'Chisinau, Moldova',  11,  6,  '[c#]', 'https://stackoverflow.com/users/2639791/hobroker', 'https://www.gravatar.com/avatar/385c16642a91030889760712ac1feb5a?s=128&d=identicon&r=PG'
'BeGiN', 'Romania',  5,  8,  '[c#]', 'https://stackoverflow.com/users/3102391/begin', 'https://www.gravatar.com/avatar/4f466a6dcd484f577f1dccc829c509e1?s=128&d=identicon&r=PG'
'ashcrok', 'Iasi, Romania',  8,  21,  '[java]', 'https://stackoverflow.com/users/2449837/ashcrok', 'https://i.stack.imgur.com/NdJEl.gif?s=128&g=1'
'CatalinM', 'Bucharest, Romania',  13,  15,  '[c#, java]', 'https://stackoverflow.com/users/1040484/catalinm', 'https://www.gravatar.com/avatar/32d968bdf8b70b1f944d455ae2c48c41?s=128&d=identicon&r=PG'
'Cristian', 'Iasi, Romania',  8,  17,  '[java]', 'https://stackoverflow.com/users/1179852/cristian', 'https://www.gravatar.com/avatar/467582690d6d4b85624c6e19098da23f?s=128&d=identicon&r=PG'
'Sorin Penteleiciuc', 'Cluj-Napoca, Cluj County, Romania',  12,  21,  '[docker, java]', 'https://stackoverflow.com/users/1603131/sorin-penteleiciuc', 'https://i.stack.imgur.com/p6cNL.jpg?s=128&g=1'
'AlexxanderX', 'Cluj-Napoca, Cluj County, Romania',  14,  10,  '[java]', 'https://stackoverflow.com/users/2115331/alexxanderx', 'https://www.gravatar.com/avatar/4d753153a2e6e1f99d255f78b17f788f?s=128&d=identicon&r=PG'
'waaadim', 'Chisinau, Moldova',  5,  13,  '[java]', 'https://stackoverflow.com/users/2147956/waaadim', 'https://www.gravatar.com/avatar/116f8f6b7451dc45dc02609fbfabd435?s=128&d=identicon&r=PG'
'Radu Cojocaru', 'Romania, Bucharest',  4,  4,  '[c#]', 'https://stackoverflow.com/users/825418/radu-cojocaru', 'https://www.gravatar.com/avatar/ff64f48e956dcbc78650b5803fb27a6b?s=128&d=identicon&r=PG'
'avat', 'Bucharest, Romania',  10,  2,  '[.net, c#]', 'https://stackoverflow.com/users/5012730/avat', 'https://www.gravatar.com/avatar/ba8d6f9420a3d504c78f1cc00faffdf4?s=128&d=identicon&r=PG'
'Catalin Florea', 'Bucharest, Romania',  7,  7,  '[java]', 'https://stackoverflow.com/users/4607546/catalin-florea', 'https://graph.facebook.com/945778972113355/picture?type=large'
'Andrei Petrut', 'Cluj-Napoca, Romania',  15,  13,  '[.net, c#]', 'https://stackoverflow.com/users/1109342/andrei-petrut', 'https://i.stack.imgur.com/uV96C.png?s=128&g=1'
'Ady Năstase', 'Romania',  1,  12,  '[c#]', 'https://stackoverflow.com/users/4234686/ady-n%c4%83stase', 'https://i.stack.imgur.com/LKEg9.jpg?s=128&g=1'
'Prisecaru Alin', 'Bucharest, Romania',  3,  2,  '[.net, c#]', 'https://stackoverflow.com/users/3012998/prisecaru-alin', 'https://graph.facebook.com/100000631930899/picture?type=large'
'roshkattu', 'Bucharest, Romania',  4,  27,  '[java]', 'https://stackoverflow.com/users/786466/roshkattu', 'https://www.gravatar.com/avatar/0ccf67551965e4dc3a795c1b9bd54089?s=128&d=identicon&r=PG'
'Adrian C.', 'Cluj-Napoca, Romania',  3,  3,  '[c#]', 'https://stackoverflow.com/users/3360540/adrian-c', 'https://i.stack.imgur.com/0SJYr.jpg?s=128&g=1'
```

In case token expired, please ping me on email for a new one: vasilelica1993@gmail.com

